//
//  InnoChatUISDK.h
//  InnoChatUISDK
//
//  Created by jagen Kabali on 29/09/21.
//

#import <Foundation/Foundation.h>
#import "UITextView+Placeholder.h"

//! Project version number for InnoChatUISDK.
FOUNDATION_EXPORT double InnoChatUISDKVersionNumber;

//! Project version string for InnoChatUISDK.
FOUNDATION_EXPORT const unsigned char InnoChatUISDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <InnoChatUISDK/PublicHeader.h>


