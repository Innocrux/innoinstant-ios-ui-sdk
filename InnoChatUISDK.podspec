Pod::Spec.new do |s|
s.name              = 'InnoChatUISDK'
s.version           = '1.0'
s.summary           = 'Description of InnoChatUISDK Framework.'

s.description      = <<-DESC
A bigger description of InnoChatUISDK Framework.
DESC

s.homepage          = 'https://www.innoinstant.com'
s.license           = "MIT"
s.authors           = { 'Jagen K' => 'jagen@innocrux.com' }
s.source            = { :git => "https://bitbucket.org/Innocrux/innoinstant-ios-ui-sdk.git", :tag => s.version }
s.vendored_frameworks = 'InnoChatUISDK.xcframework'
s.swift_version     = '5.0'

s.ios.deployment_target = '11.0'


# Add all the dependencies
s.dependency 'MFVerificationCode'
s.dependency 'AnyImageKit'
s.dependency 'SVProgressHUD'
s.dependency 'SGSegmentedProgressBarLibrary'
s.dependency 'Lightbox'
s.dependency 'FDSoundActivatedRecorder'
s.dependency 'iRecordView'
s.dependency 'Hero'
s.dependency 'GoogleMaps', '~> 4.2.0'
s.dependency 'GooglePlaces', '~> 4.2.0'

s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64', 'SWIFT_VERSION' => '5.0' }
s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }

end